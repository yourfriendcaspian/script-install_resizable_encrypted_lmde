#!/usr/bin/env bash

### This script installs Linux Mint Debian Edition v2 (aka LMDE2 aka Betsy) using an LMDE installer after optionally adding either, both, or none of
### * LUKS encryption
### * LVM2 volumes for root, swap and (optionally) data

### Note: this script must have execute permissions.

### Note on debugging:
### Stepping through code in terminal can be useful for debugging. However,
### the final `chroot` to install LMDE may not work when stepping through!

### This script is based on https://github.com/pepa65/lmdescrypt . For support:
### * if you have a specific bug report or feature request on this code, put an issue @ https://bitbucket.org/tlroche/install_resizable_encrypted_lmde/issues/new
### * if you have a more general question or comment, post to the LMDE tutorial thread @ http://forums.linuxmint.com/viewtopic.php?f=241&t=194031

############################################################################
#### CONTENTS:
#### SETUP        - mostly for logging
#### FUNCTIONS
#### MAIN         - does the install
############################################################################

############################################################################
#### SETUP
############################################################################

# set -o xtrace -o verbose -o errexit -o errtrace # if needed; I try to informatively `echo` below

### Get some information about this script from commandline

THIS_FP="${0}" # or, if stepping through this from terminal (if so, are you root?)
# set these explicitly if stepping through code
# PATH_TO_SLASH_ON_INSTALLER='/lib/live/mount/medium' # on LiveUSB, YMMV
# THIS_FP="${PATH_TO_SLASH_ON_INSTALLER}/...your path to.../install_LMDE_plus_LUKS_LVM2.sh"
THIS_PROPERTIES_RAW="${1}"
shift # since this script can take other optional arguments: see `inside` call. TODO: document!

THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))" # FQ/absolute path
THIS_FN="$(basename ${THIS_FP})"

MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## properties filename to use
## * if not specified on commandline
## * when `chroot`ing. TODO: set full path in `chroot` call
DEFAULT_PROPERTIES_FN="$(echo -e ${THIS_FN} | sed -e 's/\.sh$/.properties/')"

### Get our properties file (later to be `source`d): either specify here, or get default value (below)
THIS_PROPERTIES_FP="$(readlink -f ${THIS_PROPERTIES_RAW})" # get FQ path
# or, if stepping through this,
# THIS_PROPERTIES_FN='*** file in THIS_DIR ? properties filename here : give FQ path as THIS_PROPERTIES_FP ***'
# THIS_PROPERTIES_FP="$(readlink -f $(dirname ${THIS_FP}))/${THIS_PROPERTIES_FN}"

## get default properties file reference (if not specified above)
if [[ -z "${THIS_PROPERTIES_FP}" ]] ; then
    THIS_PROPERTIES_FN="${DEFAULT_PROPERTIES_FN}"
    THIS_PROPERTIES_FP="${THIS_DIR}/${THIS_PROPERTIES_FN}"
else
    THIS_PROPERTIES_FN="$(basename ${THIS_PROPERTIES_FP})"
fi

############################################################################
#### FUNCTIONS
############################################################################

### Just show a passed message. No real value add, except
### * single point of alteration (e.g., to add logging)
### * reuse for Debugging()
### ASSERT: if added logging: logfile exists before call
Message(){
    local MESSAGE="${1}"
    if   [[ ( -n "${log_filepath}" ) && ( -w "${log_filepath}" ) ]] ; then
        echo -e "${MESSAGE}" | tee -a "${log_filepath}"
    else
        echo -e "${MESSAGE}"
    fi
} # end Message()

### Show a message (string) that you might want to turn off (`1>/dev/null`)
Debug(){
    local MESSAGE="${1}"
    Message "${MESSAGE}"
} # end Debug()

### Put a message (string) only to log (and you might want to turn off (`1>/dev/null`))
### Name chosen for ease of editing in code (almost as easy as commenting out)
_Debug(){
    local MESSAGE="${1}"
    if   [[ ( -n "${log_filepath}" ) && ( -w "${log_filepath}" ) ]] ; then
        echo -e "${MESSAGE}" 2>&1 >> "${log_filepath}"
# else error?
    fi
} # end _Debug()

### `exit` with passed message (string) and errorcode/retval (must be integer)
Exit(){
    local MESSAGE="${1}"
    local ERROR_CODE="${2}"
    if   [[ ( -n "${log_filepath}" ) && ( -w "${log_filepath}" ) ]] ; then
        echo -e "${MESSAGE}" 1>&2 | tee -a "${log_filepath}"
    else
        echo -e "${MESSAGE}" 1>&2
    fi
    exit "${ERROR_CODE}"
} # end Exit()

Mkfs(){ ## ${1}=fs ${2}=label ${3}=dev ${4}=kind
    case ${1} in
        hfs|hfsplus|jfs|minix|msdos|vfat|ntfs) ! [[ ${4} = data ]] && Exit "${ERROR_PREFIX} A ${1} filesystem is not supported on ${4}" 2 ;;&
        ## Continue even if the above pattern matched
        ext2|ext3|ext4) mkfs.${1} -F -L "${2}" "${3}" ;;
        hfs) mkfs.${1} -h -v "${2}" "${3}" ;;
        hfsplus) mkfs.${1} -J -v "${2}" "${3}" ;;
        jfs) mkfs.${1} -q -L "${2}" "${3}" ;;
        minix) mkfs.${1} "${3}" ;;
        msdos|vfat) mkfs.${1} -n "${2}" "${3}" ;;
        ntfs) mkfs.${1} -f -F -U -L "${2}" "${3}" ;;
        reiserfs) mkfs.${1} -ffl "${2}" "${3}" ;;
        xfs) mkfs.${1} -f -L "${2}" "${3}" ;;
        *) Exit "${ERROR_PREFIX} Unsupported filesystem ${1} for ${4}-device ${3} with label ${2}" 3 ;;
    esac
} # end Mkfs()

Encrypt_managed_device(){
    local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
    local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
    local RETVAL=0
    local THIS_START="$(date)"
    
    # this should not take long!
    Debug "${MESSAGE_PREFIX} starting"
    Debug "\tcryptsetup luksFormat ${cryptopts} ${managed_device}"
    Debug "\t${THIS_START}"

    # don't quote ${cryptopts}
# `cryptsetup luksFormat` hangs if `tee`d. TODO: fix
# I would assume this is because it's prompting for input, but
# I don't have the same problem with the `cryptsetup luksOpen` below (which also prompts).
#    if   [[ ( -n "${log_filepath}" ) && ( -w "${log_filepath}" ) ]] ; then
#       cryptsetup luksFormat ${cryptopts} "${managed_device}" 2>&1 | tee -a "${log_filepath}"
#    else
        cryptsetup luksFormat ${cryptopts} "${managed_device}"
#    fi

    Debug "${MESSAGE_PREFIX}   end='$(date)'"
    Debug "${MESSAGE_PREFIX} start='${THIS_START}'"
} # end Encrypt_managed_device()

### Setup
Init(){
    local cself="${BASH_SOURCE}"
    local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
    local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
    local RETVAL=0
    mode="${1}"
    username="${2}"
    grub_device="${3}"
    lmde="${4}"
    shift 4
    packages=${*}

    rself="$(readlink -e ${cself})"
    self="$(basename ${cself})"
    ((EUID)) && Exit "${ERROR_PREFIX} This script requires root privileges. Run like: 'sudo ${BASH_SOURCE}'" 4

    if   [[ -z "${THIS_PROPERTIES_FP}" ]] ; then
        Exit "${ERROR_PREFIX} properties file path THIS_PROPERTIES_FP is not defined" 5
    elif [[ ! -r "${THIS_PROPERTIES_FP}" ]] ; then
        Exit "${ERROR_PREFIX} properties filepath='${THIS_PROPERTIES_FP}' does not exist or is not readable" 6
    else
        Message "${MESSAGE_PREFIX} reading properties='${THIS_PROPERTIES_FP}'"
        source "${THIS_PROPERTIES_FP}"
        ## log_filepath defined in properties, so 
        if   [[ -z "${log_filepath}" ]] ; then
            Message "${MESSAGE_PREFIX} no logging"
        elif [[ -r "${log_filepath}" ]] ; then
            Exit "${ERROR_PREFIX} logfile='${log_filepath}' exists: move or delete, then rerun" 7
        else
            ## don't `Message` here: we want to test logfile writing (and just writing, not `-a`)
            echo -e "${MESSAGE_PREFIX} writing to logfile='${log_filepath}'" > "${log_filepath}"
            RETVAL="${?}"
            if (( RETVAL != 0 )) ; then
                Exit "${ERROR_PREFIX} cannot write logfile='${log_filepath}'" 8
            fi
        fi
    fi

    ### chroot

    if [[ "${mode}" = 'inside' ]] ; then
        ## Inside the chroot of the new system on ${target}
        if ((lmde)) ; then
            if [[ -d '/lib/live/mount' ]] ; then
                Exit "${ERROR_PREFIX} Not in chroot, can't call 'inside': ${self} $*" 9
            fi
        else # !LMDE
            if [[ -d '/cdrom' ]] ; then
                Exit "${ERROR_PREFIX} Not in chroot, can't call 'inside': ${self} $*" 10
            fi
        fi

        NET_PROMPT="About to install LMDE with

username   ='${username}'
grub_device='${grub}'
lmde       ='${lmde}'
packages   ='${packages}'

LMDE install will require internet access to download packages. Ensure you have internet access, then press any key to continue ..."
        # separate log write from console `read`
        _Debug "${MESSAGE_PREFIX} ${NET_PROMPT}"
        read -p "

${NET_PROMPT}

"

        Debug "${MESSAGE_PREFIX} mount -t sysfs sys /sys"
        mount -t sysfs sys /sys
        Debug "${MESSAGE_PREFIX} mount -t proc proc /proc"
        mount -t proc proc /proc
        Debug "${MESSAGE_PREFIX} mount -t devpts pts /dev/pts"
        mount -t devpts pts /dev/pts
        export PS1="${prompt}" # from properties
        Debug "${MESSAGE_PREFIX} locale-gen --purge --no-archive"
        locale-gen --purge --no-archive
        Debug "${MESSAGE_PREFIX} useradd -ms /bin/bash ${username}"
        useradd -ms /bin/bash "${username}"
        until passwd "${username}" ; do : ; done # do nothing til `passwd` setting succeeds

        Debug "${MESSAGE_PREFIX} addgroup ${username} sudo"
        addgroup "${username}" sudo
        Debug "${MESSAGE_PREFIX} passwd -l root"
        passwd -l root
        Debug "${MESSAGE_PREFIX} dpkg --configure -a"
        dpkg --configure -a
        Debug "${MESSAGE_PREFIX} dpkg-reconfigure keyboard-configuration"
        dpkg-reconfigure keyboard-configuration
        Debug "${MESSAGE_PREFIX} dpkg-reconfigure tzdata"
        dpkg-reconfigure tzdata
        Debug "${MESSAGE_PREFIX} apt-get update"
        apt-get update
        Debug "${MESSAGE_PREFIX} rm -f /initrd.img"
        rm -f /initrd.img

        if [[ -n "${packages}" ]] ; then
            Debug "${MESSAGE_PREFIX} apt-get --yes --force-yes --reinstall install ${packages}"
            apt-get --yes --force-yes --reinstall install ${packages}
        fi

        if ((lmde)) ; then
            Debug "${MESSAGE_PREFIX} apt-get --yes purge '^live-.*'"
            apt-get --yes purge '^live-.*'
        fi

        Debug "${MESSAGE_PREFIX} update-grub"
        update-grub
        Debug "${MESSAGE_PREFIX} grub-install --recheck --force ${grub_device}"
        grub-install --recheck --force "${grub_device}"
        if ((lmde)) ; then
            rm -rvf '/lib/live'
        fi

        Debug "${MESSAGE_PREFIX} umount /dev/pts"
        umount /dev/pts
        Debug "${MESSAGE_PREFIX} umount /proc"
        umount /proc
        Debug "${MESSAGE_PREFIX} umount /sys"
        umount /sys

        exit 0
    fi # end inside chroot

    ## Outside the chroot of the new system on ${target}

    if [[ -w "${boot_device}" ]] ; then
        Debug "${MESSAGE_PREFIX} boot_device='${boot_device}'"
    else
        Exit "${ERROR_PREFIX} Boot device does not exist or is not writable: ${boot_device}" 11
    fi

    if [[ -w "${managed_device}" ]] ; then
        Debug "${MESSAGE_PREFIX} managed_device='${managed_device}'"
    else
        Exit "${ERROR_PREFIX} Managed (LUKS || LVM2) device does not exist or is not writable: ${managed_device}" 12
    fi

    if [[ -w "${grub_device}" ]] ; then
        Debug "${MESSAGE_PREFIX} grub_device=${grub_device}"
    else
        Exit "${ERROR_PREFIX} Grub device does not exist or is not writable: ${grub_device}" 13
    fi

    if [[ ${username} =~ ^[a-z_][a-z0-9_]*$ ]] ; then
        Debug "${MESSAGE_PREFIX} username='${username}'"
    else
        Exit "${ERROR_PREFIX} Illegal format for username: ${username}" 14
    fi

    if [[ ${hostname} =~ ^[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]$ ]] ; then
        Debug "${MESSAGE_PREFIX} hostname='${hostname}'"
    else
        Exit "${ERROR_PREFIX} Illegal format for hostname: ${hostname}" 15
    fi

    if   [[ -z "${crypt_label}" ]] ; then
        Exit "${ERROR_PREFIX} Undefined crypt_label" 16
    elif [[ "${crypt_label}" != "${crypt_label/ /}" ]] ; then
        Exit "${ERROR_PREFIX} Label 'crypt_label' contains spaces: ${crypt_label}" 17
    elif [[ -e "/dev/${crypt_label}" ]] ; then
        Exit "${ERROR_PREFIX} Device /dev/${crypt_label} exists, Volume Group name ${crypt_label} unavailable" 18
    else
        Debug "${MESSAGE_PREFIX} crypt_label='${crypt_label}'"
    fi

    if   [[ -z "${boot_label}" ]] ; then
        Exit "${ERROR_PREFIX} Label boot_label must be filled in!" 19
    elif [[ "${boot_label}" != "${boot_label/ /}" ]] ; then
        Exit "${ERROR_PREFIX} Label 'boot_label' contains spaces: ${boot_label}" 20
    else
        Debug "${MESSAGE_PREFIX} boot_label='${boot_label}'"
    fi

    if [[ -z "${boot_format}" ]] ; then
        boot_fs="$(lsblk "${boot_device}" -no FSTYPE)"
        if [[ -z "${boot_fs}" ]] ; then
            Exit "${ERROR_PREFIX} Boot partition ${boot_device} is not formatted; format first or fill in 'boot_format'" 21
        fi
    else
        Debug "${MESSAGE_PREFIX} boot_format='${boot_format}'"
        boot_fs="${boot_format}"
    fi

    Debug "${MESSAGE_PREFIX} boot_size='${boot_size}'"
    Debug "${MESSAGE_PREFIX} boot_fs='${boot_fs}'"

    if   [[ -z "${root_label}" ]] ; then
        Exit "${ERROR_PREFIX} Label root_label must be filled in!" 22
    elif [[ "${root_label}" != "${root_label/ /}" ]] ; then
        Exit "${ERROR_PREFIX} Label 'root_label' contains spaces: ${root_label}" 23
    else
        Debug "${MESSAGE_PREFIX} root_label='${root_label}'"
    fi

    if [[ -z "${root_format}" ]] ; then
        root_fs="$(lsblk "${root_device}" -no FSTYPE)"
        if [[ -z "${root_fs}" ]] ; then
            Exit "${ERROR_PREFIX} Root volume is not formatted; format first or fill in 'root_format'" 24
        fi
    else
        Debug "${MESSAGE_PREFIX} root_format='${root_format}'"    
        root_fs="${root_format}"
    fi

    # `-z "${swap_label}"` is allowed -> don't make/use that volume
    if [[ -n "${swap_label}" ]] ; then
        if   [[ "${swap_label}" != "${swap_label/ /}" ]] ; then
            Exit "${ERROR_PREFIX} Label 'swap_label' contains spaces: ${swap_label}" 25
        elif [[ ( -z "${swap_size}" ) && ( ! -b "${swap_device}" ) ]] ; then
            Exit "${ERROR_PREFIX} There is no pre-existing swap volume='${swap_label}', and no swap_size provided to make one" 26
        else
            Debug "${MESSAGE_PREFIX} swap_label='${swap_label}'"
        fi
    fi

    # `-z "${data_label}"` is allowed -> don't make/use that volume
    # TODO: cleanup this logic
    if [[ -n "${data_label}" ]] ; then
        Debug "${MESSAGE_PREFIX} data_label='${data_label}'"
        if   [[ "${data_label}" != "${data_label/ /}" ]] ; then
            Exit "${ERROR_PREFIX} Label 'data_label' contains spaces: ${data_label}" 27
        elif [[ ( -z "${data_size}" ) && ( ! -b "${data_device}" ) ]] ; then
            Exit "${ERROR_PREFIX} There is no pre-existing data volume='${data_label}', and no data_size provided to make one" 28
        elif [[ ( -z "${data_format}" ) && ( ! -b "${data_device}" ) ]] ; then
            Exit "${ERROR_PREFIX} Cannot use a newly created data volume without formatting, fill in 'data_format'" 29
        else
            if [[ -z "${data_format}" ]] ; then
                data_fs="$(lsblk "${data_device}" -no FSTYPE)"
            else
                data_fs="${data_format}"
            fi
        fi

        if [[ -z "${data_fs}" ]] ; then
            Exit "${ERROR_PREFIX} Data volume is not formatted; format first or fill in 'data_format'" 30
        fi

        Debug "${MESSAGE_PREFIX} data_size='${data_size}'"
        Debug "${MESSAGE_PREFIX} data_format='${data_format}'"
        Debug "${MESSAGE_PREFIX} data_fs='${data_fs}'"
        Debug "${MESSAGE_PREFIX} data_device='${data_device}'"
    fi

    # `-z "${root_size}"` is allowed -> don't make/use that volume
    # TODO: parse {label, size, format} options separately

    if [[ ( -n "${root_size}" ) && ( "${root_size}" != 'REST' ) && ! ( ${root_size} =~ [1-9][0-9]*[MG] ) ]] ; then
        Exit "${ERROR_PREFIX} Invalid format root_size: ${root_size}" 31
    else
        Debug "${MESSAGE_PREFIX} root_size='${root_size}'"
        Debug "${MESSAGE_PREFIX} root_fs='${root_fs}'"
        Debug "${MESSAGE_PREFIX} root_device='${root_device}'"
    fi

    if [[ "${data_size}" = 'REST' ]] ; then
        if [[ "${root_size}" = 'REST' ]] ; then
            Exit "${ERROR_PREFIX} data_size and root_size can't both be 'REST'" 32
        else
            lvmdata='-l 100%FREE -Zy'
        fi
    else
        if [[ -n "${data_size}" ]] ; then
            if [[ ${data_size} =~ [1-9][0-9]*[MG] ]] ; then
                lvmdata="-L ${data_size} -Zy"
            else
                Exit "${ERROR_PREFIX} Invalid format data_size: ${data_size}" 33
            fi
        fi
    fi
    Debug "${MESSAGE_PREFIX} lvmdata='${lvmdata}'"

    if [[ "${swap_size}" = 'DEFAULT' ]] ; then
        new_swap_size="$(($(head -1 /proc/meminfo |sed 's/[^0-9]//g')+100000))K"
        Debug "${MESSAGE_PREFIX} swap_size=='DEFAULT' -> swap size=='${new_swap_size}'"
        lvmswap="-L ${new_swap_size} -Cy -Zy"
    else
        if [[ -n "${swap_size}" ]] ; then
            Debug "${MESSAGE_PREFIX} swap_size='${swap_size}'"
            if [[ ${swap_size} =~ [1-9][0-9]*[MG] ]] ; then
                lvmswap="-L ${swap_size} -Cy -Zy"
            else
                Exit "${ERROR_PREFIX} Invalid format swap_size: ${swap_size}" 34
            fi
        fi
    fi

    Debug "${MESSAGE_PREFIX} swap_format='${swap_format}'"
    Debug "${MESSAGE_PREFIX} swap_fs='${swap_fs}'"
    Debug "${MESSAGE_PREFIX} swap_device='${swap_device}'"
    Debug "${MESSAGE_PREFIX} lvmswap='${lvmswap}'"

    if ((force_random)) ; then
        force_reencrypt=1
    fi

    if [[ -d '/lib/live' ]] ; then  ## Debian Edition
        lmde=1
        rofs="${rofsde}"
    else                            ## regular Mint
        lmde=0
        rofs="${rofslm}"
    fi
    Debug "${MESSAGE_PREFIX} rofs='${rofs}'"
} # end Init()

### Encrypt partitions (as needed/wanted)
LUKS(){
    local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
    local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
    local RETVAL=0

    if ((force_reencrypt)) ; then
        if ((force_random)) ; then
            Debug "${MESSAGE_PREFIX} badblocks -c 10240 -s -w -t random -v ${managed_device}"
            badblocks -c 10240 -s -w -t random -v "${managed_device}" 2>&1 | tee -a "${log_filepath}"
        fi
        Encrypt_managed_device
    else
        if [[ $(cryptsetup isLuks "${managed_device}") ]] ; then
            Message "\n${MESSAGE_PREFIX} ${managed_device} is already formatted, reencryption not forced"
        else
            Encrypt_managed_device
        fi
    fi

    ## Decrypt if not yet mapped

    if [[ ! -b "${crypt_device}" ]] ; then
        Debug "${MESSAGE_PREFIX} cryptsetup luksOpen ${managed_device} ${crypt_label}"
        cryptsetup luksOpen "${managed_device}" "${crypt_label}" 2>&1 | tee -a "${log_filepath}"
    fi
    crypt_uuid="$(blkid -s UUID -o value "${managed_device}")"
    Debug "${MESSAGE_PREFIX} crypt_uuid='${crypt_uuid}'"
} # end LUKS()

### Make LVM2 volumes and format (as needed/wanted)
LVM2(){
    local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
    local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
    local RETVAL=0

    if [[ $(grep " ${crypt_label}$" <<<"$(vgs --noheadings -o vg_name)") ]] ; then
        :
    else
        Debug "${MESSAGE_PREFIX} vgcreate -Zy ${crypt_label} ${crypt_device}"
        vgcreate -Zy "${crypt_label}" "${crypt_device}" 2>&1 | tee -a "${log_filepath}"
    fi
    Debug "${MESSAGE_PREFIX} vgchange --refresh"
    vgchange --refresh 2>&1 | tee -a "${log_filepath}"

    [[ -h "${root_device}" ]] && root_nomap=0 || root_nomap=1
    [[ -h "${swap_device}" ]] && swap_nomap=0 || swap_nomap=1
    [[ -h "${data_device}" ]] && data_nomap=0 || data_nomap=1

    if [[ -n "${swap_size}" ]] ; then
        if ((swap_nomap)) ; then
            Debug "${MESSAGE_PREFIX} lvcreate ${lvmswap} -n ${swap_label} ${crypt_label}"
            # don't quote ${lvmswap}
            lvcreate ${lvmswap} -n "${swap_label}" "${crypt_label}" 2>&1 | tee -a "${log_filepath}"
        fi
    fi

    if [[ -n "${swap_label}" ]] ; then
        Debug "${MESSAGE_PREFIX} mkswap -f -L ${swap_label} ${swap_device}"
        mkswap -f -L "${swap_label}" "${swap_device}" 2>&1 | tee -a "${log_filepath}"
    fi

    if [[ "${root_size}" != 'REST' ]] ; then
        if ((root_nomap)) ; then
            Debug "${MESSAGE_PREFIX} lvcreate -L ${root_size} -Zy -n ${root_label} ${crypt_label}"
            lvcreate -L "${root_size}" -Zy -n "${root_label}" "${crypt_label}" 2>&1 | tee -a "${log_filepath}"
        fi
    fi

    if [[ "${data_size}" ]] ; then
        if ((data_nomap)) ; then
            Debug "${MESSAGE_PREFIX} lvcreate ${lvmdata} -n ${data_label} ${crypt_label}"
            # don't quote ${lvmdata}
            lvcreate ${lvmdata} -n "${data_label}" "${crypt_label}" 2>&1 | tee -a "${log_filepath}"
        fi
        if [[ "${data_format}" ]] ; then
            Debug "${MESSAGE_PREFIX} Mkfs $data_format ${data_label} ${data_device} data"
            Mkfs "${data_format}" "${data_label}" "${data_device}" data 2>&1 | tee -a "${log_filepath}"
        fi
    fi

    if [[ "${root_size}" = 'REST' ]] ; then
        if ((root_nomap)) ; then
            Debug "${MESSAGE_PREFIX} lvcreate -l 100%FREE -Zy -n ${root_label} ${crypt_label}"
            lvcreate -l 100%FREE -Zy -n "${root_label}" "${crypt_label}" 2>&1 | tee -a "${log_filepath}"
        fi
    fi

    if [[ "${root_format}" ]] ; then
        Debug "${MESSAGE_PREFIX} Mkfs ${root_format} ${root_label} ${root_device} root"
        Mkfs "${root_format}" "${root_label}" "${root_device}" root 2>&1 | tee -a "${log_filepath}"
    fi

    if [[ "${boot_format}" ]] ; then
        Debug "${MESSAGE_PREFIX} Mkfs ${boot_format} ${boot_label} ${boot_device} boot"
        Mkfs "${boot_format}" "${boot_label}" "${boot_device}" boot 2>&1 | tee -a "${log_filepath}"
    fi
    boot_uuid="$(blkid -s UUID -o value "${boot_device}")"
    Debug "${MESSAGE_PREFIX} boot_uuid='${boot_uuid}'"
} # end LVM2()

### Prepare filesystems, e.g., by `mount`ing them
Prepare_filesystems(){
    local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
    local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
    local RETVAL=0

    if [[ -n "${swap_label}" ]] ; then
        Debug "${MESSAGE_PREFIX} swapon ${swap_device}"
        swapon "${swap_device}" 2>&1 | tee -a "${log_filepath}"
    fi
    Debug "${MESSAGE_PREFIX} swapon -s"
    swapon -s 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX} mkdir -p ${target}"
    mkdir -p "${target}" 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX} mount ${root_device} ${target}"
    mount "${root_device}" "${target}" 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX} mkdir -p ${target}/boot"
    mkdir -p "${target}/boot" 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX} mount ${boot_device} ${target}/boot"
    mount "${boot_device}" "${target}/boot" 2>&1 | tee -a "${log_filepath}"

    # added for logfile
    log_dir="$(dirname "${log_filepath}")"
    Debug "${MESSAGE_PREFIX} mkdir -p ${target}/${log_dir}"
    mkdir -p "${target}/${log_dir}" 2>&1 | tee -a "${log_filepath}"
    # mount "${log_dir}" "${target}/${log_dir}" # gets error='/media/mint/... is not a block device'
    Debug "${MESSAGE_PREFIX} mount ${log_device} ${target}/${log_dir}"
    mount "${log_device}" "${target}/${log_dir}" 2>&1 | tee -a "${log_filepath}"

    if [[ -n "${data_label}" ]] ; then
        Debug "${MESSAGE_PREFIX} mkdir -p ${target}/${data_label}"
        mkdir -p "${target}/${data_label}" 2>&1 | tee -a "${log_filepath}"
        Debug "${MESSAGE_PREFIX} mount ${data_device} ${target}/${data_label}"
        mount "${data_device}" "${target}/${data_label}" 2>&1 | tee -a "${log_filepath}"
    fi
    Debug "${MESSAGE_PREFIX} df --sync -hlT -x tmpfs -x devtmpfs"
    df --sync -hlT -x tmpfs -x devtmpfs 2>&1 | tee -a "${log_filepath}"

    ## Copy read-only filesystem
    Debug "${MESSAGE_PREFIX} starting"
    Debug "\tcp -a ${rofs}/* ${target}"
    Debug "\t${THIS_START}"
    # cp -a "${rofs}/*" "${target}" 2>&1 | tee -a "${log_filepath}"
    # don't quote the splat! shell needs to see it to glob
    cp -a "${rofs}"/* "${target}" 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX}   end='$(date)'"
    Debug "${MESSAGE_PREFIX} start='${THIS_START}'"

    ## Prepare root

    FSTAB="${target}/etc/fstab"
    if [[ -w "${FSTAB}" ]] ; then
        cat <<-END > "${FSTAB}"
proc /proc proc defaults 0 0
${swap_device} ${swap_label} swap sw 0 0
${root_device} / ${root_fs} relatime,errors=remount-ro 0 1
UUID=${boot_uuid} /boot ${boot_fs} relatime 0 2
END
    else
        Exit "${ERROR_PREFIX} cannot write fstab='${FSTAB}'" 35
    fi

    if [[ -n "${data_label}" ]] ; then
        echo "${data_device} /${data_label} $data_fs relatime 0 2" >> "${FSTAB}"
    fi

    # TODO: also test writability of these files
    echo "${crypt_label} UUID=$crypt_uuid none luks" > "${target}/etc/crypttab"
    sed "s/mint/${hostname}/g" /etc/hosts > "${target}/etc/hosts"
    sed "s/mint/${username}/g" /etc/mdm/mdm.conf > "${target}/etc/mdm/mdm.conf"
    echo "${hostname}" > "${target}/etc/hostname"
    # echo "$LANG.UTF-8" >> "${target}/etc/locale.gen"  # gets `error: Bad entry 'en_US.UTF-8.UTF-8 '
    echo "${LANG} UTF-8" >> "${target}/etc/locale.gen"
    Debug "${MESSAGE_PREFIX} rm -- ${target}/etc/resolv.conf"
    rm -- "${target}/etc/resolv.conf"  ## remove symlink
    Debug "${MESSAGE_PREFIX} cp /etc/resolv.conf ${target}/etc"
    cp /etc/resolv.conf "${target}/etc"

    # start debug
    echo | tee -a "${log_filepath}" # newline
    for FP in \
        "${FSTAB}" \
        "${target}/etc/crypttab" \
        "${target}/etc/hosts" \
        "${target}/etc/mdm/mdm.conf" \
        "${target}/etc/hostname" \
        "${target}/etc/locale.gen" \
        "${target}/etc/resolv.conf" \
    ; do
        Debug "${MESSAGE_PREFIX} ls -al ${FP}"
        ls -al "${FP}" 2>&1 | tee -a "${log_filepath}"
        Debug "${MESSAGE_PREFIX} wc -l ${FP}"
        wc -l < "${FP}" 2>&1 | tee -a "${log_filepath}"
        Debug "${MESSAGE_PREFIX} cat ${FP}"
        cat "${FP}" 2>&1 | tee -a "${log_filepath}"
        echo | tee -a "${log_filepath}" # newline
    done
    #   end debug

    Debug "${MESSAGE_PREFIX} cp ${rself} ${target}/root/${self}"
    cp "${rself}" "${target}/root/${self}" 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX} mount --bind /dev ${target}/dev"
    mount --bind /dev "${target}/dev" 2>&1 | tee -a "${log_filepath}"
} # end Prepare_filesystems()

### Finish up in the fresh install
Install_filesystems(){
    local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
    local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
    local RETVAL=0

    # Make properties readable inside chroot: just copy, since read-only is OK
    THIS_PROPERTIES_OLD="${THIS_PROPERTIES_FP}"
    # THIS_PROPERTIES_FP="${target}/root/${THIS_PROPERTIES_FN}"
    # cp "${THIS_PROPERTIES_OLD}" "${THIS_PROPERTIES_FP}"
    # ASSERT: ${target}/ only exists pre-`chroot`
    THIS_PROPERTIES_FP="/root/${THIS_PROPERTIES_FN}"
    Debug "${MESSAGE_PREFIX} cp ${THIS_PROPERTIES_OLD} ${target}/${THIS_PROPERTIES_FP}"
    cp "${THIS_PROPERTIES_OLD}" "${target}/${THIS_PROPERTIES_FP}" 2>&1 | tee -a "${log_filepath}"

    Message 'About to `chroot` with commandline='
    # PePas original
    # chroot "${target}" "/root/${self}" inside ${username} ${grub_device} $lmde ${packages}
    Message "chroot '${target}' '/root/${self}' '${THIS_PROPERTIES_FP}' 'inside' '${username}' '${grub_device}' '${lmde}' '${packages}'"
    chroot "${target}" "/root/${self}" "${THIS_PROPERTIES_FP}" 'inside' "${username}" "${grub_device}" "${lmde}" "${packages}"

    Debug "${MESSAGE_PREFIX} umount ${target}/dev"
    umount "${target}/dev" 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX} umount ${target}/boot"
    umount "${target}/boot" 2>&1 | tee -a "${log_filepath}"
    if [[ -n "${data_size}" ]] ; then
        Debug "${MESSAGE_PREFIX} umount ${target}/${data_label}"
        umount "${target}/${data_label}" 2>&1 | tee -a "${log_filepath}"
    fi
    Debug "${MESSAGE_PREFIX} umount ${target}"
    umount "${target}" 2>&1 | tee -a "${log_filepath}"
    if [[ -n "${swap_size}" ]] ; then
        Debug "${MESSAGE_PREFIX} swapoff ${swap_device}"
        swapoff "${swap_device}" 2>&1 | tee -a "${log_filepath}"
    fi
    Debug "${MESSAGE_PREFIX} sync"
    sync 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX} sleep 1"
    sleep 1 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX} vgchange -an"
    vgchange -an 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX} sync"
    sync 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX} sleep 1"
    sleep 1 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX} cryptsetup luksClose ${crypt_label}"
    cryptsetup luksClose "${crypt_label}" 2>&1 | tee -a "${log_filepath}"
    Debug "${MESSAGE_PREFIX} sync"
    sync 2>&1 | tee -a "${log_filepath}"

} # end Install_filesystems()

############################################################################
#### MAIN
############################################################################

THIS_START="$(date)"
Debug "${MESSAGE_PREFIX} starting"

# "Init ${*}" # because `chroot` call will need same arguments as this=$0
for F in \
    "Init ${*}" \
    'LUKS' \
    'LVM2' \
    'Prepare_filesystems' \
    'Install_filesystems' \
; do
    Debug "${MESSAGE_PREFIX} ${F}"
    eval "${F}" # TODO: move logging here? to simplify code
    RETVAL="${?}"
    if (( RETVAL != 0 )) ; then
        Exit "${ERROR_PREFIX} '${F}' exited with errorcode='${RETVAL}'" 1
    fi
done

Debug "${MESSAGE_PREFIX}   end='$(date)'"
Debug "${MESSAGE_PREFIX} start='${THIS_START}'"
Message "\n${MESSAGE_PREFIX} Ready for reboot!\n"
exit 0
